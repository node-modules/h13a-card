module.exports = function(task) {

  var identity = `fui-card`;

  $(task.widget.element).data(identity, {});

  $.expr[":"][identity] = function(elem) {
    return !!$.data(elem, identity);
  };

  task.resolve();
};
