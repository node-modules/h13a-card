module.exports = function(task) {

  task.widget._on(task.widget.element, {
    "click a[data-action], button[data-action]": function(event) {
      event.preventDefault();
      let actionButton = $(event.target);
      let action = actionButton.data('action');
      let actions = task.widget.util.parseUnixCommand(action);
      console.log('Parsed Actions', actions);
      task.widget._action_trigger({
        data: actions
      });
    },
  });
  task.resolve();
};
