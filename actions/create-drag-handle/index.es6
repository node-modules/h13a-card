


module.exports = function(task) {

  task.widget.dragHandle = $(`<div class="card-block card-text drag-handle"><button class="btn btn-sm btn-info drag-close"><i class="fa fa-caret-square-o-down"></i></button></div>`);
  task.widget.element.append(task.widget.dragHandle);

  task.resolve();

};
