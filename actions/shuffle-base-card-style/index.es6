module.exports = function(task) {
  if (task.widget.option("cardStyle") == "*") {
    let shuffledCardStyle = task.widget.util.random(0, task.widget.option("cardStyleUpper"));
    task.widget.option("cardStyle", shuffledCardStyle);
    task.widget.util.setNumericClassValue({
      prefix: task.widget.option("cardStyleClass") + "-style",
      element: task.widget.element,
      number: shuffledCardStyle,
    });
  }
  task.resolve();
};

