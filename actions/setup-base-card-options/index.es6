module.exports = function(task) {
  let base = {

    parentId: -1,

    speed: 0,
    effect: 1,

    cardType: "card",
    cardHeight: false,

    cardContext: "success",

    cardTitle: "",
    cardSubtitle: "",

    cardIcon: "file-text",
    cardLink: null,

    cardStyle: "*",
    cardStyleUpper: 3,
    cardStyleClass: 'fui-card',

    cssClasses: '',

    showTitlebar: true,
    showActions: true,
    showConsole: true,

    // runtime, information show/hide (pro mode)
    lessInfo: false,


    actions: {},

  };

  task.widget.option($.extend(true, base, task.widget.option()));

  task.resolve();
};
