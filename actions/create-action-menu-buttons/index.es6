module.exports = function(task) {

  let actions = task.widget.util.otaoi(task.widget.option("actions"));


  //console.log(`Action Load ${task.info.name}.${task.card.name}: ${actions.length}`);

  if (actions.length) {

    // TODO: add a sorting algorithm, make use of [group, order] properties.

    let $block = $(`<div class="card-text actions-position"></div>`);
    let $actions = $(`<div class="card-actions"></div>`);

    actions.forEach(action => {

      let $action = $(`<span class="card-action"><i title="${action.title||action.id}" class="card-action-icon fa fa-${action.icon||action.id} glow-muted p-x-1"></i></span>`);
      $actions.append($action);

      task.widget._on($action, {
        "click": () => {

          //console.log( `Action ${action.id} trigger clicked` )

          let localAction = task.widget['_action_' + action.id];
          let localActionExists = !!localAction;

          if (localActionExists) {

            //console.log( `Action ${action.id} will be handeled locally.` )

            // Execute Button
            localAction({}, {
              data: action.data
            });

            if (action.id.match(/^toggle_/)) {
              // this is a toggle action
              if ($action.hasClass('glow-warning')) {
                $action.removeClass('glow-warning');
              } else {
                $action.addClass('glow-warning');
              }
            }

          } else if (action.data) {
            //console.log( `Action ${action.id} has data, will be sent to terminal-services.` );

            if (typeof action.data == 'string') {
              action.data = action.data.replace(/\$GUID/g, task.widget.guid);
            } else if (action.data.map) {
              action.data = action.data.map(i => i.replace(/\$GUID/g, task.widget.guid));
            }

            task.widget._command_send({
              data: action.data
            });

          } else {
            console.log('Command was not local, did not have a data field.');

            let cmd = 'ux_' + (action.id || task.widget.widgetName);
            let str = `${cmd} --guid "${task.widget.guid}"`;
            task.widget._command_send({
              data: str
            });

          }

        }
      });

    });

    $block.append($actions);

    task.widget.elementActionsBlock.empty().append($block);
  }
  task.resolve();
};
