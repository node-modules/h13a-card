module.exports = function(task) {

  task.widget.console = $(`<div class="card-block card-text console-position"></div>`);
  task.widget.element.append(task.widget.console);
  task.resolve();

};
