module.exports = function(task) {

  // NOTE: for each block in a card
  task.widget.blockDestroy.forEach(block => {

    // NOTE: if block has .destructor then it will be executed.
    let blockHasDestructor = ( block.destructor );

    if (blockHasDestructor) block.destructor();

  });

  // console.info('Blocks Destroyed');
  task.resolve();
};
