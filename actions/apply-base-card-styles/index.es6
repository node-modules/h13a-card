module.exports = function(task) {

  $(task.widget.element).addClass(`card card-inverse ${task.widget.option("cardStyleClass")}`);

  if (task.widget.option("cardHeight")) {
    $(task.widget.element).addClass(`height-${task.widget.option("cardHeight")}`);
  }

  $(task.widget.element).addClass(task.widget.option("cardStyleClass") + '-style' + task.widget.option("cardStyle"));
  $(task.widget.element).addClass(task.widget.option("cssClasses"));

  task.resolve();
};
