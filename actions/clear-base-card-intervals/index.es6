module.exports = function(task) {

  task.widget.intervalDestroy.forEach(interval => {
    clearInterval(interval);
  });

  //console.info('Intervals Cleared');
  task.resolve();
};
