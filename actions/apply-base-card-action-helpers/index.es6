module.exports = function(task) {

  task.widget._action_trigger = function(packet) {

    packet.data.map(o => {
      if (task.widget['_action_' + o.cmd]) task.widget['_action_' + o.cmd]({}, {
        data: o.data
      });
    });

  };

  task.resolve();
};
