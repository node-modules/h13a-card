module.exports = function(task) {


  const serviceMap = {};

  for (var name in task.widget) {

    if (name.match(/_service_\w/)) {
      var bits = name.split('_');
      serviceMap[bits[2]] = name;
    }
  }


  if (task.widget.util.ota(serviceMap).length) {

    task.widget._on(document, serviceMap);

    if (task.widget.console) {
      const names = task.widget.util.ota(serviceMap).map(o => `<span class="text-warning">${o.key}</span>`);
      task.widget.console.append(`<div>Remote Services: ${names.join(', ')}. <i class="fa fa-info text-info" style="cursor: default;" title="online"></i></div>`);

    }

  }


  task.resolve();
};
