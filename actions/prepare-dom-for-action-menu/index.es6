module.exports = function(task) {

  task.widget.elementActionsBlock = $(`<div class="card-block card-text action-position p-b-1"></div>`);
  task.widget.element.append(task.widget.elementActionsBlock);
  task.resolve();

};
