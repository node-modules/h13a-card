var upperFirst = require('lodash/upperFirst');


module.exports = function(task) {


  task.widget.elementTitleBlock = $(`<div style="display: block;" class="hvr-bounce-to-right card-block card-text title-position p-a-2"></div>`);

  if (task.widget.option("showTitlebar")) {
    // visible
  } else {
    task.widget.elementTitleBlock.hide();
  }

  task.widget.element.append(task.widget.elementTitleBlock);



  let interpolator = (str) => {
    return str
      .replace(/\$NAME/g, upperFirst(task.widget.packageJson.name))
      .replace(/\$VERSION/g, upperFirst(task.widget.packageJson.version));
  };

  let cardTitle = interpolator(task.widget.option("cardTitle"));
  let cardSubtitle = interpolator(task.widget.option("cardSubtitle"));

  let $content = $(`
    <div>
        <i class="fa fa-3x fa-${task.widget.option("cardIcon")}"></i>
        <h4 style="text-transform: uppercase;">${cardTitle}</h4>
        <div style="text-transform: uppercase;" class="text-muted small">${cardSubtitle}</div>
    </div>
  `);

  task.widget.elementTitleBlock.empty().append($content);

  task.resolve();
};
