const Promise = require("bluebird");
const DEBUG = true;

// this is an action that creates blocks
// blocks are low level card constructs, focus on using the block plugin.

const metadata_block = require('h13a-metadata-block');
const plugin_block = require('h13a-plugin-block');
const file_block = require('h13a-file-block');

const blockLookup = {
  'metadata-block': metadata_block,
  'file-block': file_block,
  'plugin-block': plugin_block,
};

module.exports = function(task) {


  let blocks = task.widget.option("blocks");
  //  if(DEBUG) console.log( `Attempting to load blocks of ${blocks}` )

  let blocksExist = (blocks && blocks.length);

  if(blocksExist) {

    let convertBlockToTask = (block) => {

      //  if(DEBUG) console.log( `Attempting to load block.id of ${block.id}` )

      let BlockObject = blockLookup[block.id + '-block'];

      if(BlockObject){

      // an action in a card is seeding a block
      // if block is a plugin, then that is yet to load a jq-*
      let blockObject = new BlockObject({task, block});

      // add to destructor array
      // if a block has a destructor method then it will be executed when card is destroyed
      task.widget.blockDestroy.push( blockObject );

      // Create Must Return Promise
      let taskPromise = blockObject.creator();

      return taskPromise;

    }else{

      return new Promise((resolve) => { resolve(); });

    }

    };

    Promise.each(blocks, convertBlockToTask).done(() => {

      // console.log("BLOCKS FINISHED");
      task.resolve();
    });

  }else{

    // no blocks to render,
    task.resolve();
  }

};
