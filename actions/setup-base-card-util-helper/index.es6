module.exports = function(task) {

  const util = require("h13a-util");
  let packageJson = require("../../../../package.json");

  task.widget.util = util;
  task.widget.packageJson = packageJson;
  task.resolve();
};
