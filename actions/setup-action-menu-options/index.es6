module.exports = function(task) {
  let enhance = {

    actions: {
      cs: {
        icon: "wrench",
        "title": "Show Next Card Style",
        data: "cs --guid $GUID --up --floor 0 --ceiling 3"
      }
    }

  };
  task.widget.option($.extend(true, task.widget.option(), enhance));
  task.resolve();
};

