module.exports = function(task) {

  task.widget.body = $(`<div class="card-block card-text body-position"></div>`);
  task.widget.element.append(task.widget.body);
  task.resolve();

};
