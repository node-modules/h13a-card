module.exports = function(task) {

  let trigger = function(event) {

    event.preventDefault();

    let commandButton = $(event.target);

    let data = commandButton.data('command');

    if(!data) return; // Old WebKit throws here, exit early to avoid unusual problems.

    data = data.replace( /\%%GUID/g, task.widget.guid);

    let commandObject = {
      meta: {
        guid: task.widget.guid,
        uuid: task.widget.uuid
      },
      data
    };

    task.widget._command_send(commandObject);


  };
  task.widget._on(task.widget.element, { "click a[data-command], button[data-command]": trigger });

  task.resolve();
};
