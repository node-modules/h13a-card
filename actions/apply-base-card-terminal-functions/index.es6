module.exports = function(task) {
  task.widget._command_send = function(packet) {
    $(document).trigger('terminal', packet);
  };
  task.widget._command_reply = function(to, packet) {
    $(document).trigger(to, packet);
  };
  task.resolve();
};
