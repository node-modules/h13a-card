module.exports = function(task) {

  $("a[data-command], button[data-command]", task.widget.element).each(function(index, element){

    let autorun = $(this).data('autorun');
    let commands = $(this).data('command');

    if(autorun && commands){

      //console.log(autorun && commands)

      setTimeout(function(){
        $(document) .trigger( 'terminal', {data: commands} );
      }, autorun);

    }

  });

  task.resolve();
};
