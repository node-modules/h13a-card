// var shortid = require('shortid');
module.exports = function(task) {

  task.widget.guid = task.widget.option('guid') || 'g' + task.widget.uuid;

  $(task.widget.element).addClass(task.widget.guid);

  task.resolve();
};
