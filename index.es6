const Factory = require('h13a-factory');

const info = require("../../package.json");
const card = require("./package.json");
const flow = require("./flow.json");

const code = require.context("./actions/", true, /[a-z0-9-]+\/index.es6$/);

class Card {

constructor({widgetFullName, options }) {
    this.widget = {
      options: options,

      _create: function() {
        this._superApply(arguments);
        this.factory = new Factory({widget: this, info, card, flow, code});
        this.control = this.factory.getControl({});
        this.control.run( {action: 'create'} );
      },

    }
    // as soon as the constructor is called, the Query-ui widget will come to life.
    $.widget( widgetFullName , this.widget );
  }

}

module.exports = Card;
